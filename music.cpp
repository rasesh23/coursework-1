/*
    music.cpp
    Author ID : M00788514
    Created on : 08/01/2022
    Updated on : 14/01/2022
*/

#include "events.h"
#include "music.h"

Music::Music(int max_standing) : Event("Events")
{
    this->max_standing = max_standing;
    this->count_standing = 0;
}

//Function to book music event
void Music::bookMusic(int musictickets)
{
    this->count_standing = this->count_standing + musictickets;

    if (this->count_standing > this->max_standing)
    {
        std::cout << "Sorry all places have been booked, no more tickets can be purchased\n" << std::endl;
    }

    else
    {
        std::cout << " Live Music is booked, Thank You!\n" << std::endl;
    }
}

//Function to cancel booking for music event
void Music::cancelMusic(int musictickets)
{
    this->count_standing = this->count_standing - musictickets;

    if (this->count_standing == 0)
    {
        std::cout << " Invalid cancellation, event is not booked\n" << std::endl;
    }

    else if (this->count_standing < musictickets)
    {
        std::cout << " Invalid cancellation, number of booking is less\n" << std::endl;
    }
    
    else
    {
        std::cout << " Booking for live music has been cancelled. Thank You!\n" << std::endl;
    }
}

//Function to display details about music event
void Music::musicDetails()
{
    this->music_available = this->max_standing - this->count_standing;

    std::cout << " \n The music event has a capacity of " <<this->max_standing << " places and has " <<this->music_available << " places available\n\n ";
}