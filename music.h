/*
    music.h
    Author ID : M00788514
    Created on : 08/01/2022
    Updated on : 14/01/2022
*/

#ifndef _MUSIC_H_
#define _MUSIC_H_

#include "events.h"
#include <string>

class Music : public Event
{
    //Private member variables
    private:
    int count_standing;
    int max_standing;
    int music_available;

    //Public member functions
    public:
    Music(int max_standing);
    void bookMusic(int musictickets);
    void cancelMusic(int musictickets);
    void musicDetails();
};

#endif