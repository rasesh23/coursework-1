CXX = g++
CXXFLAGS = -g -Wall -Wextra

.PHONY: all
all: Theatre

Theatre: Theatre.cpp events.o music.o comedy.o film.o film3d.o
	$(CXX) $(CXXFLAGS) -o $@ $^

music.o: music.cpp events.o
	$(CXX) $(CXXFLAGS) -c $^

comedy.o: comedy.cpp events.o
	$(CXX) $(CXXFLAGS) -c $^

film.o: film.cpp events.o
	$(CXX) $(CXXFLAGS) -c $^

film3d.o: film3d.cpp events.o
	$(CXX) $(CXXFLAGS) -c $^

events.o: events.cpp
	$(CXX) $(CXXFLAGS) -c $^

.PHONY: clean
clean:
	rm *~ *.o Theatre.exe
