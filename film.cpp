/*
    film.cpp
    Author ID : M00788514
    Created on : 08/01/2022
    Updated on : 14/01/2022
*/

#include "events.h"
#include "film.h"

Film::Film(int max_seating_2D) : Event("Events")
{
    this->max_seating_2D = max_seating_2D;
    this->count_seating_2D = 0;
}

//Function to book film event 2D version
void Film::bookfilm2D(int film2D)
{
    this->count_seating_2D = this->count_seating_2D + film2D;

    if (this->count_seating_2D > this->max_seating_2D)
    {
        std::cout << " Sorry all places have been booked, no more tickets can be purchased\n" << std::endl;
    }

    else
    {
        std::cout << " Film (2D version) booked, Thank You!\n" << std::endl;
    }
}

//Function to cancel film event 2D version
void Film::cancelfilm2D(int film2D)
{
    this->count_seating_2D = this->count_seating_2D - film2D;

    if (this->count_seating_2D == 0)
    {
        std::cout << " Invalid cancellation, event is not booked\n" << std::endl;
    }

    else if (this->count_seating_2D < film2D)
    {
        std::cout << " Invalid cancellation, number of booking is less\n" << std::endl;
    }
    
    else
    {
        std::cout << " Booking for film (2D version) has been cancelled. Thank You!\n" << std::endl;
    }
}

//Function to display details about film event
void Film::filmDetails()
{
    this->film_available_2D = this->max_seating_2D - this->count_seating_2D;

    std::cout << "The film event is available in 2 different versions, namely 2D and 3D viewing" << std::endl;
    std::cout << " The 2D film event has a seating capacity of " <<this->max_seating_2D << " and has " <<this->film_available_2D << " seats available\n ";
}