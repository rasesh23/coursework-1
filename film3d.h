/*
    film3d.h
    Author ID : M00788514
    Created on : 08/01/2022
    Updated on : 14/01/2022
*/

#ifndef _FILM3D_H_
#define _FILM3D_H_

#include "events.h"
#include <string>

class Film3d : public Event
{
    //Private member variables
    private:
    int max_seating_3D;
    int count_seating_3D;
    int film_available_3D;

    //Public member functions
    public:
    Film3d(int max_seating_3D);
    void bookfilm3D(int film3D);
    void cancelfilm3D(int film3D);

    void filmDetails();
};

#endif