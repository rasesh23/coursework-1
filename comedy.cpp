/*
    comedy.cpp
    Author ID : M00788514
    Created on : 08/01/2022
    Updated on : 14/01/2022
*/

#include "events.h"
#include "comedy.h"

Comedy::Comedy(int max_comedy) : Event("Events")
{
    this->max_comedy = max_comedy;
    this->count_comedy = 0;
}

//Function to book stand-up comedy event
void Comedy::bookcomedy(int comedyseat)
{
    this->count_comedy = this->count_comedy + comedyseat;

    if (this->count_comedy > this->max_comedy)
    {
        std::cout << "Sorry all seats have been booked, no more tickets can be purchased\n" << std::endl;
    }

    else
    {
        std::cout << "Stand-up Comedy is booked, Thank You!\n" << std::endl;
    }
}

//Function to cancel booking for music event
void Comedy::cancelcomedy(int comedyseat)
{
    this->count_comedy = this->count_comedy - comedyseat;

    if (this->count_comedy == 0)
    {
        std::cout << " Invalid cancellation, event is not booked\n" << std::endl;
    }

    else if (this->count_comedy < comedyseat)
    {
        std::cout << " Invalid cancellation, number of booking is less\n" << std::endl;
    }
    
    else
    {
        std::cout << " Booking for stand-up comedy has been cancelled. Thank You!\n" << std::endl;
    }
}

//Function to display details about stand-up comedy event
void Comedy::comedyDetails()
{
    this->comedy_available = this->max_comedy - this->count_comedy;

    std::cout << "The stand-up comedy event allows seat selection, has a seating capacity of " <<this->max_comedy << " and has " <<this->comedy_available << " seats available\n\n  ";
}