/*
    film3d.cpp
    Author ID : M00788514
    Created on : 08/01/2022
    Updated on : 14/01/2022
*/

#include "events.h"
#include "film3d.h"

Film3d::Film3d(int max_seating_3D) : Event("Events")
{
    this->max_seating_3D = max_seating_3D;
    this->count_seating_3D = 0;
}

//Function to book film event 3D version
void Film3d::bookfilm3D(int film3D)
{
    this->count_seating_3D = this->count_seating_3D + film3D;

    if (this->count_seating_3D > this->max_seating_3D)
    {
        std::cout << "Sorry all places have been booked, no more tickets can be purchased\n" << std::endl;
    }

    else
    {
        std::cout << "Film (3D version) booked, Thank You! \n" << std::endl;
    }
}

//Function to cancel film event 3D version
void Film3d::cancelfilm3D(int film3D)
{
    this->count_seating_3D = this->count_seating_3D - film3D;

    if (this->count_seating_3D == 0)
    {
        std::cout << " Invalid cancellation, event is not booked\n" << std::endl;
    }

    else if (this->count_seating_3D < film3D)
    {
        std::cout << " Invalid cancellation, number of booking is less\n" << std::endl;
    }
    
    else
    {
        std::cout << " Booking for film (3D version) has been cancelled. Thank You!\n" << std::endl;
    }
}

//Function to display details about film event
void Film3d::filmDetails()
{
    this->film_available_3D = this->max_seating_3D - this->count_seating_3D;
    std::cout << "The 3D film event has a seating capacity of " <<this->max_seating_3D << " and has " <<this->film_available_3D << " seats available\n\n ";
}