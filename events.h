/*
    events.h
    Author ID : M00788514
    Created on : 08/01/2022
    Updated on : 14/01/2022
*/

#ifndef _EVENTS_H_
#define _EVENTS_H_

#include <string>
#include <iostream>

class Event
{
    //Private member variables
    private:
    std:: string name;

    //Public member functions
    public:
    Event(std::string name);
    void printDetails();
    virtual ~Event();
};

#endif
