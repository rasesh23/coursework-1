/*
    comedy.h
    Author ID : M00788514
    Created on : 08/01/2022
    Updated on : 14/01/2022
*/

#ifndef _COMEDY_H_
#define _COMEDY_H_

#include "events.h"
#include <string>

class Comedy : public Event
{
    private:
    int max_comedy;
    int count_comedy;
    int comedy_available;

    public:
    Comedy(int max_comedy);
    void bookcomedy(int comedyseat);
    void cancelcomedy(int comedyseat);
    void comedyDetails();
};

#endif