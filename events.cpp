/*
    events.cpp
    Author ID : M00788514
    Created on : 08/01/2022
    Updated on : 13/01/2022
*/

#include "events.h"

Event::Event(std::string name)
{
  this->name = name;
}

//Function to display the list of all events
void Event::printDetails()
{
  std::cout << "\n The RJ Theatre has 3 events, namely : \n "<< std::endl;
  std::cout << " 1. Live music "<< std::endl;
  std::cout << " 2. Stand-up comedy "<< std::endl;
  std::cout << " 3. Film "<< std::endl;
}

Event:: ~Event()
{

}
