/*
    music.h
    Author ID : M00788514
    Created on : 08/01/2022
    Updated on : 14/01/2022
*/

#ifndef _FILM_H_
#define _FILM_H_

#include "events.h"
#include <string>

class Film : public Event
{
    //Private member variables
    private:
    int max_seating_2D;
    int count_seating_2D;
    int film_available_2D;

    //Public member functions
    public:
    Film(int max_seating_2D);
    void bookfilm2D(int film2D);
    void cancelfilm2D(int film2D);

    void filmDetails();
};

#endif