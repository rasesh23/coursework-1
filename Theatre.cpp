/*
    Theatre.cpp
    Author ID : M00788514
    Created on : 08/01/2022
    Updated on : 14/01/2022
*/

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <unistd.h>

//Including the header files
#include "events.h"
#include "music.h"
#include "comedy.h"
#include "film.h"
#include "film3d.h"

//Menu details
void Menu()
{
    std::cout << " \n\n Welcome to RJ Theater. Please select your desire choice: \n " << std::endl;
    std::cout << "------------------------------------------------------------ \n";
    std::cout << " 1. Add a booking for an event " << std::endl;
    std::cout << " 2. Cancel/Refund a booking " << std::endl;
    std::cout << " 3. List all events " << std::endl;
    std::cout << " 4. List details and Availability of a given event " << std::endl;
    std::cout << " 5. Load data from a file (all events and availability) " << std::endl;
    std::cout << " 6. Save data to file (all events and availability) " << std::endl;
    std::cout << " 7. Exit " << std::endl;
    std::cout << "------------------------------------------------------------ \n";
}

//Function for event choice and choice
void MenuChoice(std::string str)
{
	std::cout << " 1. "<<str<<" Live Music " << std::endl;
	std::cout << " 2. " << str << " Stand-up comedy " << std::endl;
	std::cout << " 3. " << str << " Film " << std::endl;
	std::cout << " 4. Back to Main Menu \n" << std::endl;
}

int main()
{
	int choice; 
    bool flag = true; 
    int musictickets;
    int comedyseat;
    int version;
    int film2D;
    int film3D;

    Event event = Event("Events");
    Music music = Music(300);
    Comedy comedy = Comedy(200);
    Film film = Film(200);
    Film3d film3d = Film3d(200);

    while (true)
    {
        Menu();
        std::cout << " Enter your Choice : ";
		std::cin >> choice;

        switch (choice)
		{
            //In case 1, we are looking at the booking process
            case 1:
            {
                while (flag)
				{
                    MenuChoice("Add");
					std::cout << " Enter your Choice : ";
					std::cin >> choice;
					switch (choice)
                    {
                        //Booking tickets for live music event
                        case 1:
                        {
                            std::cout << " Enter number of tickets you wish to purchase: ";
                            std::cin >> musictickets;

                            if (musictickets > 300)
                            {
                                std::cout << " Invalid number, cannot exceed 300 \n" << std::endl;
                            }

                            else
                            {
                                music.bookMusic(musictickets);
                            }
						    break;
                        }

                        //Booking tickets for stand-up comedy event
                        case 2:
                        {
                            std::cout << " Enter number of tickets you wish to purchase: ";
                            std::cin >> comedyseat;

                            if (comedyseat > 200)
                            {
                                std::cout << " Invalid number, cannot exceed 200 \n" << std::endl;
                            }

                            else
                            {
                                comedy.bookcomedy(comedyseat);
                            }
                            break;
                        }

                        //Booking tickets for film event
                        case 3:
                        {
                            std::cout << " \n Film is available in 2 versions: "<< std::endl;
                            std::cout << " 1. 2D version "<< std::endl;
                            std::cout << " 2. 3D version " << std::endl;
                            std::cout << std::endl;

                            std::cout << " Choose the version you would like: ";
                            std::cin >> version;

                            if (version == 1)
                            {
                                std::cout << " Enter number of tickets you wish to purchase: ";
                                std::cin >> film2D;

                                if (film2D > 200)
                                {
                                    std::cout << " Invalid number, cannot exceed 200 \n" << std::endl;
                                }

                                else
                                {
                                    film.bookfilm2D(film2D);
                                }
                                break;
                            }

                            else
                            {
                                std::cout << " Enter number of tickets you wish to purchase: ";
                                std::cin >> film3D;

                                if (film3D > 200)
                                {
                                    std::cout << " Invalid number, cannot exceed 200 \n" << std::endl;
                                }

                                else
                                {
                                    film3d.bookfilm3D(film3D);
                                }
                            }
                            
							break;
                        }

                        //Returning back to the main menu
                        case 4:
                        {
                            flag = false;
                            break;
                        }

                        default:
                        {
							sleep(50);
							std::cout << " Number you entered is out of range " << std::endl;
							break;
                        }

                    }
					  
                }

                flag = true;
				break;		  
            }

            //In case 2, we are looking at the cancellation process
            case 2:
            {
                while (flag)
				{
                    MenuChoice("Cancel");
					std::cout << " Enter your Choice : ";
					std::cin >> choice;
					switch (choice)
                    {
                        //Cancellation process for live music event
                        case 1:
                        {
                            std::cout << "Enter number of tickets you wish to cancel: ";
                            std::cin >> musictickets;

                            music.cancelMusic(musictickets);
                            break;
                        }

                        //Cancellation process for stand-up comedy event
                        case 2:
                        {
                            std::cout << "Enter number of tickets you wish to cancel: ";
                            std::cin >> comedyseat;

                            comedy.cancelcomedy(comedyseat);
                            break;
                        }

                        //Cancellation process for film event
                        case 3:
                        {

                            std::cout << "Film are available in 2 versions: " << std::endl;
                            std::cout << " 1. 2D version " << std::endl;
                            std::cout << " 2. 3D version " << std::endl;

                            std::cout << "Choose the version you would cancel: ";
                            std::cin >> version;
                            std::cout << std::endl;

                            if (version == 1)
                            {
                                std::cout << "Enter number of tickets you wish to cancel: ";
                                std::cin >> film2D;

                                film.cancelfilm2D(film2D);
                            }

                            else
                            {
                                std::cout << "Enter number of tickets you wish to cancel: ";
                                std::cin >> film3D;

                                film3d.cancelfilm3D(film3D);
                            }
                            break;
                        }

                        //Returning back to the main menu
                        case 4:
                        {
                            flag = false;
                            break;
                        }

                        default:
                        {
							sleep(50);
							std::cout << " Number you entered is out of range " << std::endl;
							break;
                        }

                    }

                }   
                flag = true;
			    break;
            }

            //In case 3, we are looking at the list of events
            case 3:
            {
                event.printDetails();
                break;
            }

            //In case 4, we are looking at each event and its details
            case 4:
            {

                while (flag)
				{
                    MenuChoice("Details about");
					std::cout << " Enter your Choice : ";
					std::cin >> choice;
					switch (choice)
                    {
                        //Details about live music event
                        case 1:
                        {
                            music.musicDetails();
                            break;
                        }

                        //Details about stand-up comedy
                        case 2:
                        {
                            comedy.comedyDetails();
                            break;
                        }

                        //Cancellation process for film event
                        case 3:
                        {
                            film.filmDetails();
                            film3d.filmDetails();
                            break;
                        }

                        //Returning back to the main menu
                        case 4:
                        {
                            flag = false;
                            break;
                        }

                        default:
                        {
							sleep(50);
							std::cout << " Number you entered is out of range " << std::endl;
							break;
                        }
                    }
                }
                flag = true;
			    break;
            }

            //In case 5, we are looking to load data from a file
            case 5:
            {
                std::ifstream MyFile ("Event.txt");
                std::string line;

                while (std::getline(MyFile,line))
                {
                    std::cout << line << std::endl;
                }

                std::cout << std::endl;
                std::cout << "Your Data is Loaded Successfully!";
                std::cout << std::endl <<std::endl;

                /*std::ofstream outFile ("Events.txt");

                outFile << music.musicDetails(1);
                outFile << comedy.comedyDetails(1);
                outFile << film.filmDetails(1);
                outFile << film3d.filmDetails(1);

                outFile.close();*/
                break;
            }

            //In case 6, we are looking to save data from a file
            case 6:
            {
                /*std::ofstream file;

                file.open("Event.txt");
                file << "Writing data\n";

                file << music.musicDetails();
                file << comedy.comedyDetails();
                file << film.filmDetails();
                file << film3d.filmDetails();

                file.close();
                std::cout << "Your Data is Saved Now! Thank You!" << std::endl;
				break;*/

                //std::ofstream MyFile("Events.txt", std::ios::app);



				break;
            }

            case 7:
		    {
                //Exit the whole program
                exit(1);   
		    }

            default:   // invalid number
		    {
				std::cout << " Number you entered is out of range "<<std::endl;
				break;
		    }
		}

	}
	return 0;
}